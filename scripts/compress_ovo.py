import csv
import datetime
from dateutil.relativedelta import relativedelta

data_file = open("OVO-Data-Looped.csv")
data_file_reader = csv.reader(data_file, delimiter=',')
next(data_file_reader)

new_data_file = open("OVO-Data-Modified.csv", mode="w")
new_data_file_writer = csv.writer(new_data_file)

compressed_users = 500

i = 0
l = 15847470

for row in data_file_reader:
    print(f"{i}/{l} {i/l * 100}%")

    q = int(row[0]) // compressed_users

    data_date = datetime.datetime.strptime(row[1], "%Y-%m-%d %H:%M:%S")

    new_data_file_writer.writerow([(int(row[0]) % (compressed_users+1)) + (1 if q != 0 else 0),
                                   data_date + relativedelta(years=q),
                                   row[2]])

    i += 1

data_file.close()
new_data_file.close()

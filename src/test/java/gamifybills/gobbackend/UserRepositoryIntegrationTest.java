package gamifybills.gobbackend;

import static org.assertj.core.api.Assertions.assertThat;

import gamifybills.gobbackend.model.User;
import gamifybills.gobbackend.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIntegrationTest {

  @Autowired
  private UserRepository userRepository;
  @Test
  public void whenFindByUsername_thenReturnUser() {
    // Given
    User test = new User("test", "pass");
    userRepository.save(test);
    userRepository.flush();

    // When
    User found = userRepository.findByUsername(test.getUsername()).get();

    // Then
    assertThat(found.getUsername())
        .isEqualTo(test.getUsername());
  }
}


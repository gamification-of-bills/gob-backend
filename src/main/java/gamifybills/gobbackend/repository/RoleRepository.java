package gamifybills.gobbackend.repository;

import gamifybills.gobbackend.model.Role;
import gamifybills.gobbackend.model.RoleName;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  Optional<Role> findByName(RoleName roleName);
}

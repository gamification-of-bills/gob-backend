package gamifybills.gobbackend.payload;

import gamifybills.gobbackend.model.EnergyUsage;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

public class MonthStatsResponse {

  private final LocalDateTime startDate;

  private final DoubleSummaryStatistics stats;

  public MonthStatsResponse(LocalDateTime startDate, List<EnergyUsage> dataPoints) {
    this.startDate = startDate;

    List<Double> months = new ArrayList<>(dataPoints.stream()
        .collect(Collectors.groupingBy((EnergyUsage e) -> (e.getEnergyUsageID().getUsername()),
            Collectors.summingDouble(EnergyUsage::getConsumption))).values());

    this.stats = months.stream().collect(Collectors.summarizingDouble(Double::doubleValue));

  }

  public LocalDateTime getStartDate() {
    return startDate;
  }

  public Double getAverage() {
    return stats.getAverage();
  }

  public Double getMin() {
    return stats.getMin();
  }

  public Double getMax() {
    return stats.getMax();
  }

}
